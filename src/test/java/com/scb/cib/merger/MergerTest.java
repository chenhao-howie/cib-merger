package com.scb.cib.merger;

import com.scb.cib.merger.impl.CompositeMerger;
import com.scb.cib.merger.impl.LeafMerger;
import com.scb.cib.model.Instrument;
import com.scb.cib.rule.impl.LmeMappingRule;
import com.scb.cib.rule.impl.LmeMergingRule;
import com.scb.cib.rule.impl.LmePrimeMappingRule;
import com.scb.cib.rule.impl.LmePrimeMergingRule;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MergerTest {

    List<Instrument> instruments = null;
    Instrument lme = null;
    Instrument prime = null;

    @Before
    public void setup() {
        instruments = new ArrayList<>();
        lme = new Instrument();
        lme.setExchange("LME");
        lme.setInstrumentCode("PB_03_2018");
        lme.setLastTradingDate("15-03-2018");
        lme.setDeliveryDate("17-03-2018");
        lme.setMarket("PB");
        lme.setLabel("Lead 13 March 2018");
        instruments.add(lme);
        prime = new Instrument();
        prime.setExchange("PRIME");
        prime.setInstrumentCode("PRIME_PB_03_2018");
        prime.setLastTradingDate("14-03-2018");
        prime.setDeliveryDate("18-03-2018");
        prime.setMarket("LME_PB");
        prime.setLabel("Lead 13 March 2018");
        prime.setExchangeInstrumentCode("PB_03_2018");
        prime.setTradable("FALSE");
        instruments.add(prime);
    }

    @Test
    public void testLmeMerge() {
        Instrument instrument = new LeafMerger(new LmeMappingRule(), new LmeMergingRule(), instruments).merge(lme);
        assertEquals(lme.getExchange(), instrument.getExchange());
        assertEquals(lme.getInstrumentCode(), instrument.getInstrumentCode());
        assertEquals(lme.getLastTradingDate(), instrument.getLastTradingDate());
        assertEquals(lme.getDeliveryDate(), instrument.getDeliveryDate());
        assertEquals(lme.getMarket(), instrument.getMarket());
        assertEquals(lme.getLabel(), instrument.getLabel());
        assertNull(lme.getExchangeInstrumentCode());
        assertNull(instrument.getExchangeInstrumentCode());
        assertEquals("TRUE", instrument.getTradable());
    }

    @Test
    public void testCompositeLmeMerge() {
        CompositeMerger compositeMerger = new CompositeMerger();
        compositeMerger.addMerger(new LeafMerger(new LmeMappingRule(), new LmeMergingRule(), instruments));
        Instrument instrument = compositeMerger.merge(lme);
        assertEquals(lme.getExchange(), instrument.getExchange());
        assertEquals(lme.getInstrumentCode(), instrument.getInstrumentCode());
        assertEquals(lme.getLastTradingDate(), instrument.getLastTradingDate());
        assertEquals(lme.getDeliveryDate(), instrument.getDeliveryDate());
        assertEquals(lme.getMarket(), instrument.getMarket());
        assertEquals(lme.getLabel(), instrument.getLabel());
        assertNull(lme.getExchangeInstrumentCode());
        assertNull(instrument.getExchangeInstrumentCode());
        assertEquals("TRUE", instrument.getTradable());
    }

    @Test
    public void testPrimeMerge() {
        Instrument instrument = new LeafMerger(new LmePrimeMappingRule(), new LmePrimeMergingRule(), instruments).merge(prime);
        assertEquals(prime.getExchange(), instrument.getExchange());
        assertEquals(prime.getInstrumentCode(), instrument.getInstrumentCode());
        assertEquals(lme.getLastTradingDate(), instrument.getLastTradingDate());
        assertEquals(lme.getDeliveryDate(), instrument.getDeliveryDate());
        assertEquals(instrument.getMarket(), "PB");
        assertEquals(prime.getLabel(), instrument.getLabel());
        assertEquals(prime.getExchangeInstrumentCode(), instrument.getExchangeInstrumentCode());
        assertEquals("FALSE", instrument.getTradable());
    }

    @Test
    public void testCompositePrimeMerge() {
        CompositeMerger compositeMerger = new CompositeMerger();
        compositeMerger.addMerger(new LeafMerger(new LmePrimeMappingRule(), new LmePrimeMergingRule(), instruments));
        Instrument instrument = compositeMerger.merge(prime);
        assertEquals(prime.getExchange(), instrument.getExchange());
        assertEquals(prime.getInstrumentCode(), instrument.getInstrumentCode());
        assertEquals(lme.getLastTradingDate(), instrument.getLastTradingDate());
        assertEquals(lme.getDeliveryDate(), instrument.getDeliveryDate());
        assertEquals(instrument.getMarket(), "PB");
        assertEquals(prime.getLabel(), instrument.getLabel());
        assertEquals(prime.getExchangeInstrumentCode(), instrument.getExchangeInstrumentCode());
        assertEquals("FALSE", instrument.getTradable());
    }

    @Test
    public void testCompositeLmePrimeMerge() {
        CompositeMerger lmeCompositeMerger = new CompositeMerger();
        lmeCompositeMerger.addMerger(new LeafMerger(new LmeMappingRule(), new LmeMergingRule(), instruments));
        Instrument instrument = lmeCompositeMerger.merge(lme);
        assertEquals("TRUE", instrument.getTradable());
        CompositeMerger primeCompositeMerger = new CompositeMerger();
        primeCompositeMerger.addMerger(lmeCompositeMerger);
        primeCompositeMerger.addMerger(new LeafMerger(new LmePrimeMappingRule(), new LmePrimeMergingRule(), instruments));
        instrument = primeCompositeMerger.merge(prime);
        assertEquals(prime.getExchange(), instrument.getExchange());
        assertEquals(prime.getInstrumentCode(), instrument.getInstrumentCode());
        assertEquals(lme.getLastTradingDate(), instrument.getLastTradingDate());
        assertEquals(lme.getDeliveryDate(), instrument.getDeliveryDate());
        assertEquals(instrument.getMarket(), "PB");
        assertEquals(prime.getLabel(), instrument.getLabel());
        assertEquals(prime.getExchangeInstrumentCode(), instrument.getExchangeInstrumentCode());
        assertEquals("FALSE", instrument.getTradable());
    }

}