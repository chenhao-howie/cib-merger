package com.scb.cib.model;

import com.scb.cib.util.StringUtil;

import java.util.StringJoiner;

public class Instrument implements Cloneable {
    private String exchange;
    private String instrumentCode;
    private String lastTradingDate;
    private String deliveryDate;
    private String market;
    private String label;
    private String exchangeInstrumentCode;
    private String tradable;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getInstrumentCode() {
        return instrumentCode;
    }

    public void setInstrumentCode(String instrumentCode) {
        this.instrumentCode = instrumentCode;
    }

    public String getLastTradingDate() {
        return lastTradingDate;
    }

    public void setLastTradingDate(String lastTradingDate) {
        this.lastTradingDate = lastTradingDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getExchangeInstrumentCode() {
        return exchangeInstrumentCode;
    }

    public void setExchangeInstrumentCode(String exchangeInstrumentCode) {
        this.exchangeInstrumentCode = exchangeInstrumentCode;
    }

    public String getTradable() {
        return tradable;
    }

    public void setTradable(String tradable) {
        this.tradable = tradable;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Instrument) {
            Instrument instrument = (Instrument) obj;
            return StringUtil.equals(this.exchange, instrument.exchange)
                    && StringUtil.equals(this.instrumentCode, instrument.instrumentCode)
                    && StringUtil.equals(this.lastTradingDate, instrument.lastTradingDate)
                    && StringUtil.equals(this.deliveryDate, instrument.deliveryDate)
                    && StringUtil.equals(this.market, instrument.market)
                    && StringUtil.equals(this.label, instrument.label)
                    && StringUtil.equals(this.exchangeInstrumentCode, instrument.exchangeInstrumentCode)
                    && StringUtil.equals(this.tradable, instrument.tradable);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = this.exchange != null ? hash * 31 + this.exchange.hashCode() : hash;
        hash = this.instrumentCode != null ? hash * 31 + this.instrumentCode.hashCode() : hash;
        hash = this.lastTradingDate != null ? hash * 31 + this.lastTradingDate.hashCode() : hash;
        hash = this.deliveryDate != null ? hash * 31 + this.deliveryDate.hashCode() : hash;
        hash = this.market != null ? hash * 31 + this.market.hashCode() : hash;
        hash = this.label != null ? hash * 31 + this.label.hashCode() : hash;
        hash = this.exchangeInstrumentCode != null ? hash * 31 + this.exchangeInstrumentCode.hashCode() : hash;
        hash = this.tradable != null ? hash * 31 + this.tradable.hashCode() : hash;
        return hash;
    }

    @Override
    public String toString() {
        return new StringJoiner(",", "[", "]").add(this.exchange)
                .add(this.instrumentCode).add(this.lastTradingDate).add(this.deliveryDate)
                .add(this.market).add(this.label).add(this.exchangeInstrumentCode)
                .add(this.tradable).toString();
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new UnsupportedOperationException("Clone is not supported");
        }
    }

}
