package com.scb.cib.rule.impl;

import com.scb.cib.model.Instrument;
import com.scb.cib.rule.MergingRule;

import java.util.function.BiFunction;

public class LmeMergingRule implements MergingRule {

    @Override
    public BiFunction<Instrument, Instrument, Instrument> getRule() {
        return (lme, anything) -> {
            Instrument instrument = (Instrument) lme.clone();
            instrument.setTradable("TRUE");
            return instrument;
        };
    }

}
