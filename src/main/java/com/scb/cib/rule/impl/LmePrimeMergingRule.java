package com.scb.cib.rule.impl;

import com.scb.cib.model.Instrument;
import com.scb.cib.rule.MergingRule;

import java.util.function.BiFunction;

public class LmePrimeMergingRule implements MergingRule {

    @Override
    public BiFunction<Instrument, Instrument, Instrument> getRule() {
        return (prime, lme) -> {
            Instrument instrument = (Instrument) prime.clone();
            instrument.setLastTradingDate(lme.getLastTradingDate());
            instrument.setDeliveryDate(lme.getDeliveryDate());
            instrument.setMarket("PB");
            return instrument;
        };
    }

}
