package com.scb.cib.rule.impl;

import com.scb.cib.model.Instrument;
import com.scb.cib.rule.MappingRule;

import java.util.function.Predicate;

public class LmeMappingRule implements MappingRule {

    @Override
    public Predicate<Instrument> getRule(final Instrument instrument) {
        return lme -> "LME".equals(lme.getExchange()) && instrument != null
                && lme.getInstrumentCode() != null && lme.getInstrumentCode().equals(instrument.getInstrumentCode());
    }
}
