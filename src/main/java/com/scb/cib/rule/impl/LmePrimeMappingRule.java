package com.scb.cib.rule.impl;

import com.scb.cib.model.Instrument;
import com.scb.cib.rule.MappingRule;

import java.util.function.Predicate;

public class LmePrimeMappingRule implements MappingRule {

    @Override
    public Predicate<Instrument> getRule(final Instrument instrument) {
        return lme -> instrument != null && "PRIME".equals(instrument.getExchange())
                && "LME".equals(lme.getExchange()) && instrument.getExchangeInstrumentCode() != null
                && instrument.getExchangeInstrumentCode().equals(lme.getInstrumentCode());
    }

}
