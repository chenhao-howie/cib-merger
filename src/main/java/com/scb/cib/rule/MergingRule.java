package com.scb.cib.rule;

import com.scb.cib.model.Instrument;

import java.util.function.BiFunction;

public interface MergingRule {
    BiFunction<Instrument, Instrument, Instrument> getRule();
}
