package com.scb.cib.rule;

import com.scb.cib.model.Instrument;

import java.util.function.Predicate;

public interface MappingRule {
    Predicate<Instrument> getRule(final Instrument instrument);
}
