package com.scb.cib.merger.impl;

import com.scb.cib.merger.Merger;
import com.scb.cib.model.Instrument;

import java.util.ArrayList;
import java.util.List;

public class CompositeMerger implements Merger {

    private List<Merger> mergers = new ArrayList<>();

    public void addMerger(Merger merger) {
        this.mergers.add(merger);
    }

    @Override
    public Instrument merge(Instrument instrument) {
        for (Merger merger : mergers) {
            instrument = merger.merge(instrument);
        }
        return instrument;
    }
}
