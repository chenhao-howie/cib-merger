package com.scb.cib.merger.impl;

import com.scb.cib.merger.Merger;
import com.scb.cib.model.Instrument;
import com.scb.cib.rule.MappingRule;
import com.scb.cib.rule.MergingRule;

import java.util.List;
import java.util.Optional;

public class LeafMerger implements Merger {

    private MappingRule mappingRule;

    private MergingRule mergeRule;

    private List<Instrument> instruments;

    public LeafMerger(MappingRule mappingRule, MergingRule mergeRule, List<Instrument> instruments) {
        this.mappingRule = mappingRule;
        this.mergeRule = mergeRule;
        this.instruments = instruments;
    }

    public Instrument merge(Instrument instrument) {
        Optional<Instrument> mappedInstrument = this.instruments.stream().filter(mappingRule.getRule(instrument)).findFirst();
        if (mappedInstrument.isPresent()) {
            return mergeRule.getRule().apply(instrument, mappedInstrument.get());
        }
        return instrument;
    }
}
