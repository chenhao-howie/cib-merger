package com.scb.cib.merger;

import com.scb.cib.model.Instrument;

public interface Merger {
    Instrument merge(Instrument instrument);
}
