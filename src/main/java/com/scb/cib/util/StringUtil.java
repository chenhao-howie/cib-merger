package com.scb.cib.util;

public class StringUtil {
    public static boolean equals(String str1, String str2) {
        if (str1 == null && str2 == null) {
            return true;
        } else if (str1 != null) {
            return str1.equals(str2);
        } else {
            return str2.equals(str1);
        }
    }
}
