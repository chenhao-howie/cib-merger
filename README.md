# Project Title

CIB Instrument Merger

### Prerequisites

Build the jar using maven

```
mvn clean package
```

### Installing

Copy the ```cib-merger-1.0.jar``` into the classpath

## Running the tests

Code sample
```java
import com.scb.cib.merger.Merger;
import com.scb.cib.merger.impl.CompositeMerger;
import com.scb.cib.merger.impl.LeafMerger;
import com.scb.cib.model.Instrument;
import com.scb.cib.rule.impl.LmeMappingRule;
import com.scb.cib.rule.impl.LmeMergingRule;
import com.scb.cib.rule.impl.LmePrimeMappingRule;
import com.scb.cib.rule.impl.LmePrimeMergingRule;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        List<Instrument> instruments = new ArrayList<>();
        Instrument lme = new Instrument();
        lme.setExchange("LME");
        lme.setInstrumentCode("PB_03_2018");
        lme.setLastTradingDate("15-03-2018");
        lme.setDeliveryDate("17-03-2018");
        lme.setMarket("PB");
        lme.setLabel("Lead 13 March 2018");
        instruments.add(lme);

        Merger lmeMerger = new LeafMerger(new LmeMappingRule(), new LmeMergingRule(), instruments);
        Instrument merged = lmeMerger.merge(lme);
        System.out.println("LME    PB_03_2018 : " + lme);
        System.out.println("MERGED PB_03_2018 : " + merged);
        System.out.println("LME not equal to MERGED : " + lme.equals(merged));

        lme.setMarket("LME_PB");
        Instrument prime = new Instrument();
        prime.setExchange("PRIME");
        prime.setInstrumentCode("PRIME_PB_03_2018");
        prime.setLastTradingDate("14-03-2018");
        prime.setDeliveryDate("18-03-2018");
        prime.setMarket("LME_PB");
        prime.setLabel("Lead 13 March 2018");
        prime.setExchangeInstrumentCode("PB_03_2018");
        prime.setTradable("FALSE");
        instruments.add(prime);

        Merger primeMerger = new LeafMerger(new LmePrimeMappingRule(), new LmePrimeMergingRule(), instruments);
        CompositeMerger compositeMerger = new CompositeMerger();
        compositeMerger.addMerger(primeMerger);
        compositeMerger.addMerger(lmeMerger);
        merged = compositeMerger.merge(prime);
        System.out.println("LME    PB_03_2018 : " + lme);
        System.out.println("PRIME  PB_03_2018 : " + prime);
        System.out.println("MERGED PB_03_2018 : " + merged);
        System.out.println("PRIME not equal to MERGED : " + prime.equals(merged));

    }
}
```